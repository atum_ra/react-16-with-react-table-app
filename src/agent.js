import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import md5 from 'md5';
import { get, create } from "./Mocks/request";

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT_GET = 'https://uxcandy.com/~shapoval/test-task-backend/?developer=Roman';
const API_ROOT_EDIT = 'https://uxcandy.com/~shapoval/test-task-backend/edit';
const API_ROOT_CREATE = 'https://uxcandy.com/~shapoval/test-task-backend/create/?developer=Roman';

let mock = true;

// Also note that if one wishes to follow the more recent RFC3986 for URLs, which makes square 
// brackets reserved (for IPv6) and thus not encoded when forming something which could be part of a URL (such as a host), 
// the following code snippet may help:

function fixedEncodeURI(str) {
  return encodeURI(str).replace(/%5B/g, '[').replace(/%5D/g, ']');
}

const responseBody = res => res.body;

const requests = {
  get: url =>
    superagent.get(`${API_ROOT_GET}${url}`).then(responseBody),
  put: (id, text, status, token, signature) =>
    superagent.post(`${API_ROOT_EDIT}/${id}?developer=Roman`).field('text', text).field('status', status)
    .field('token', token).field('signature', signature).then(responseBody),
  post: (email, username, text) =>
    superagent.post(`${API_ROOT_CREATE}`).field('email', email).field('username', username).field('text', text).then(responseBody)
};

const Auth = {
  login: (email, password) => {
      if (email === "admin" && password == "123") {
        return {user: "admin", token: "beejee"};
      } else {
        return null;
      }
    }
};

const Comments = {
  all: (page, sort_field, sort_direction) => {
    let url = ``;

    if (page > -1) url = url.concat(`&page=${parseInt(page)+1}`);
    if (sort_field) url = url.concat(`&sort_field=${sort_field}`);
    if (sort_direction) url = url.concat(`&sort_direction=${sort_direction}`);
    mock = false;

    if (mock) return get;
    else 
    return requests.get(url);
  },

  edit: comment => {
    const { id, text, status } = comment;
    let requestString = `status=${status}&text=${text}&token=beejee`;
    let signature = md5(fixedEncodeURI(requestString));
    let token = "beejee";
    mock = false;

    if (mock) {
      return  comment;
    } else {
      return requests.put(id, text, status, token, signature);
    };
  },
    
  create: body => {
    mock = false;
    const {text, username, email} = body ;

    if (mock) {
      return  body;
    } else return requests.post(email, username, text);
  }
};  

export default {
  Comments,
  Auth
};
