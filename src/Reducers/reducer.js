import login from './login';
import comments from './comments';
import { reducer as reduxFormReducer } from 'redux-form';
import { combineReducers } from 'redux';

export default combineReducers({
  login,
  comments,
  form: reduxFormReducer
});