import { 
    CREATE_COMMENT, 
    EDIT_COMMENT,
    LOAD_COMMENTS} from '../Actions/constants';


export default (state = {}, action) => {
   switch (action.type) {

       case LOAD_COMMENTS:
           return { ...state, comments: action.payload };

       case CREATE_COMMENT:
           return { ...state, newComment: action.payload, comments: state.comments.concat(action.payload) };

       case EDIT_COMMENT:
           return { ...state, comments: state.comments.concat(action.payload).filter(Boolean) };

       default:
           return state;
   }
};