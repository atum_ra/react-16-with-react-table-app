import React, { Component } from 'react';
import {connect} from 'react-redux';

import Login from "./Components/Login";
import NewComment from "./Components/Form";
import ListTable from "./Components/ListTable";
import { CREATE_COMMENT } from './Actions/constants';
import agent from './agent';
import _ from "lodash";

function mapStateToProps(state) {
  console.log("state App", state)
  return {
    ...state
  }
}

const mapDispatchToProps = dispatch => ({
  createComment: (values) => {
    Promise.resolve(agent.Comments.create(values))
      .then((response) => {
        if (_.has(response, "status") && response.status == "ok") {
          dispatch({
            type: CREATE_COMMENT,
            payload: values
          });
          alert(`Added`);
        } else alert(`Unsucceeded create with response ${JSON.stringify(response)}`);
      })
  },
});

class App extends Component {

  constructor() {
    super();
    this.onCreateComment = (v) => this.props.createComment(v);
  }

  render() {
    console.log("this props App", this.props )
    return (
      <div className="App">
        <Login/>
        <ListTable/>
        <NewComment onSubmit={this.onCreateComment} />
        <br/>
        <h3 style={{fontWeight:"bold"}}> It should be done  in addition for commercial development </h3>
          <ul>
         <li>1. propTypes</li>
         <li>2. more states of app</li>
         <li>3. add more unit and integration tests </li>
         <li>3. UI Design with CSS </li>
         </ul>
        
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)